package mis.pruebas.carritoproductos.controlador;


import mis.pruebas.carritoproductos.modelo.Producto;
import mis.pruebas.carritoproductos.modelo.Usuario;
import mis.pruebas.carritoproductos.servicio.ServicioGenerico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/almacen/v1/productos/{idProducto}/usuarios")
public class ControladorUsuarioProducto {

    @Autowired
    ServicioGenerico<Producto> servicioProducto;

    static class UsuarioAux {
        public long id;
        public String nombre;
    }

    @PostMapping
    public void agregarUsuarioProducto(@PathVariable long idProducto,
                                       @RequestBody ControladorUsuarioProducto.UsuarioAux userAux) {
        Usuario usuario = new Usuario();
        usuario.setId(userAux.id);
        usuario.setNombre(userAux.nombre);
        System.out.println("ID Usuario: " + usuario.getId() + " Usuario " + usuario.getNombre());
        try {
            this.servicioProducto.obtenerPorId(idProducto)
                    .getUsuarios().add(usuario);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    /*@GetMapping
    public CollectionModel<EntityModel<Usuario>> obtenerUsuariosProducto(@PathVariable long idProducto) {
        List<Usuario> usuarios = this.servicioProducto.obtenerPorId(idProducto).getUsuarios();

        return CollectionModel.of(usuarios.stream().map(p -> EntityModel.of(p).add(linkTo(methodOn(ControladorProducto.class).obtenerProductoPorId(idProducto)).withRel("producto").withTitle("Productos"))).collect(Collectors.toList()))
                .add(linkTo(methodOn(this.getClass()).obtenerUsuariosProducto(idProducto)).withSelfRel().withTitle("Proveedores"));
    }*/


    @GetMapping
    public ResponseEntity getProductIdUsers(@PathVariable int idProducto) {
        Producto pr = servicioProducto.obtenerPorId(idProducto);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsuarios() != null)
            return ResponseEntity.ok(pr.getUsuarios());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
