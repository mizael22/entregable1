package mis.pruebas.mongodbprod.controlador;

import mis.pruebas.mongodbprod.modelo.Producto;
import mis.pruebas.mongodbprod.repository.impl.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/almacen/v2/productos")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    public List<Producto> obtenerProductos() {
        return this.servicioProducto.findAll();
    }

    @GetMapping("/{idProducto}")
    public Optional<Producto> obtenerProductoPorId(@PathVariable String idProducto) {
        Optional<Producto> prod = this.servicioProducto.findById(idProducto);
        if (prod != null)
            return prod;
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public Producto agregarProductos(@RequestBody Producto prod) {
        this.servicioProducto.save(prod);
        return prod;
    }

    @PutMapping
    public void actualizarProductoPorId(@RequestBody Producto prod) {
            this.servicioProducto.update(prod);
    }

    @DeleteMapping("/{idProducto}")
    public boolean borrarProductoPorId(@PathVariable String idProducto) {
        Optional<Producto> prodDlt = this.servicioProducto.findById(idProducto);
        if (prodDlt != null)
            return this.servicioProducto.delete(prodDlt.get());
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

}
