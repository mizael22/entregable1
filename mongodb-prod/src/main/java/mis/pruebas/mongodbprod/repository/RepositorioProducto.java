package mis.pruebas.mongodbprod.repository;

import mis.pruebas.mongodbprod.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioProducto extends MongoRepository<Producto, String> {
}
