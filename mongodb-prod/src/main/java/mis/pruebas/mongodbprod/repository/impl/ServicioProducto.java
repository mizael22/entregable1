package mis.pruebas.mongodbprod.repository.impl;

import mis.pruebas.mongodbprod.modelo.Producto;
import mis.pruebas.mongodbprod.repository.RepositorioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioProducto {

    @Autowired
    RepositorioProducto productoRepository;

    public List<Producto> findAll() {
        return this.productoRepository.findAll();
    }

    public Optional<Producto> findById(String id) {
        return this.productoRepository.findById(id);
    }

    public Producto save(Producto p) {
        p.setId(null);
        return this.productoRepository.insert(p);
    }

    public boolean update(Producto p) {
        try {
            this.productoRepository.save(p);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean delete(Producto p) {
        try {
            this.productoRepository.delete(p);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
